#60ipad-plugin
アイデザイナー用のプラグインです。

##使い方
[仕様事例](https://bitbucket.org/KAKKO-E/60ipad-server/src/b07141a311e920b07b3bc099d95dcfb60f4b97e1/views/events.ejs?at=master&fileviewer=file-view-default#events.ejs-87:92)

### 1. プラグインを追加します
`60ipad.min.js`を入れます。`dist/`に入っています。
```
<script src="/javascripts/60ipad.min.js"></script>
```

### 2. iPadControllerを初期化します。
iPadサーバのアドレスを設定する必要があります。

これはラズパイのIPになります。

ラズパイを固定IPに設定するのはおすすめです。

```
iPadController.setEndPoint('http://192.168.100.1:3000');
```

### 3. イベントの送信
```
iPadController.send('addSticker', 'sticker1');
```

### 4. まとめ
```
<script src="/javascripts/60ipad.min.js"></script>
<script>
    $(document).ready(function() {
        iPadController.setEndPoint('http://192.168.100.1:3000');
        
        $('#add-sticker').click(function () {
            iPadController.send('addSticker', 'sticker1')
                .catch(console.error);
        });
    });
</script>
```

##API詳細

### setEndPoint(url)

__url__: `string` iPadサーバーへのアドレス

### send(event, id, modifiers):Promise

__event__: `string` 送信イベント名 (例 addSticker)

__list__: `string` ターゲットのid (例 sticker1)

__modifier__: `object` イベントのプロパティー (例 {color: 'red', x: 100, y: 100})

現在は`modifiers`がオプション引数です。

現在のサーバーは受け取るようになってますが、対応していません。

将来的に使う可能性があります。

このメソットはプロミスを返します。

プロミスライブラリは`bluebird`です。

AJAX通信ライブラリーは`axios`です。

##イベントリストと事例

このイベントは対応する予定です

- `addSticker`
```javascript
iPadController.send('addSticker', 'sticker-id', {
    colors: [ '#7B658C' ]
});
```
- `addMessage`
```javascript
iPadController.send('addMessage', null, {
    color: '#7B658C',
    text: 'Test Message'
});
```

- `startPrinting`
```javascript
iPadController.send('startPrinting', null);
```

- `addPattern`
```javascript
iPadController.send('addPattern', 'pattern-id');
```

##問い合わせ
service@komoni.mx