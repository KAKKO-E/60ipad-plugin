import axios    from 'axios';
import Promise  from 'bluebird';

export default class IPadController {

    constructor() {
        this.endpoint = null;
        this.parseRequestResponse = this.parseRequestResponse.bind(this);
    }

    setEndPoint(url) {
        this.endpoint = url;
    }

    sendEvent(type, data) {
        return new Promise((resolve, reject) => {
            if (this.endpoint) {
                return axios.post(`${this.endpoint}/api/v1/event`, {name:type, ...data})
                    .then(this.parseRequestResponse)
                    .then(resolve)
                    .catch(reject);
            } else {
                reject(new Error(`Cannot make request to iPad server. Endpoint is not set.`));
            }
        });
    }

    parseRequestResponse(response) {
        return new Promise((resolve, reject) => {
            if (typeof response == 'object') {
                if (typeof response.data == 'object') {
                    if (response.data.ok) {
                        resolve(response.data);
                    } else {
                        reject(new Error(response.data.error || 'unknown error'))
                    }
                } else {
                    reject(new Error(`Received incorrect response from server.`));
                }
            } else {
                reject(new Error(`Received incorrect response from server.`));
            }
        });
    }

    send(eventName, id, modifiers) {
        return this.sendEvent(eventName, {id, modifiers});
    }
}