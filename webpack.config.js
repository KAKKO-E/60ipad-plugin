const webpack = require('webpack');

module.exports = {
    entry : "./src/index.js",
    output : {
        filename : "./dist/60ipad.min.js"
    },
    module : {
        loaders : [
            {
                test : /\.jsx?$/,
                exclude : /(node_modules|bower_components)/,
                loader : 'babel-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js']
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            minimize : true,
            compress : {
                warnings : true,
                screw_ie8 : true
            }
        })
    ]
};